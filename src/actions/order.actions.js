import { UPDATE_TYPE, UPDATE_SIZE, UPDATE_ORDER, UPDATE_DRINK, UPDATE_ORDER_CODE } from "../constants/order.constants"

export const updateType = type => {
    return {
        type: UPDATE_TYPE,
        payload: type
    }
}

export const updateSize = size => {
    return {
        type: UPDATE_SIZE,
        payload: size
    }
}

export const updateOrder = order => {
    return {
        type: UPDATE_ORDER,
        payload: order
    }
}

export const updateDrink = id => {
    return {
        type: UPDATE_DRINK,
        payload: id
    }
}

export const updateOrderCode = code => {
    return {
        type: UPDATE_ORDER_CODE,
        payload: code
    }
}
