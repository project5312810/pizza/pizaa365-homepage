const images = {
    slide1: require('./1.jpg'),
    slide2: require('./2.jpg'),
    slide3: require('./3.jpg'),
    slide4: require('./4.jpg'),
    bacon: require('./bacon.jpg'),
    hawaiian: require('./hawaiian.jpg'),
    seafood: require('./seafood.jpg'),
}

export default images