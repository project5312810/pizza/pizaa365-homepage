
import { createTheme, ThemeProvider } from "@mui/material"
import './App.css'
import Content from "./components/Content";
import Footer from "./components/Footer";
import Header from "./components/Header";
function App() {
  const theme = createTheme({
    typography: {
      h1: {
        fontSize: '2.5rem',
        fontWeight: 500
      },
      h2: {
        fontSize: '2rem',
        fontWeight: 500
      },
      h3: {
        fontSize: '1.75rem',
        fontWeight: 500
      },
      h4: {
        fontSize: '1.5rem',
        fontWeight: 500
      },
      h5: {
        fontSize: '1.25rem',
        fontWeight: 500
      },
      h6: {
        fontSize: '1rem',
        fontWeight: 500
      },
      button: {
        fontSize: '1rem'
      }
    },
  })
  return (
    <ThemeProvider theme={theme}>
      <Header />
      <Content />
      <Footer />
    </ThemeProvider>
  );
}

export default App;
