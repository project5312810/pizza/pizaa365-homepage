import { UPDATE_TYPE, UPDATE_SIZE, UPDATE_ORDER, UPDATE_DRINK, UPDATE_ORDER_CODE } from "../constants/order.constants"

const initialState = {
    order: {
        kichCo: '',
        duongKinh: '',
        suon: '',
        salad: '',
        loaiPizza: '',
        idVoucher: '',
        idLoaiNuocUong: '',
        soLuongNuoc: '',
        hoTen: '',
        thanhTien: '',
        email: '',
        soDienThoai: '',
        diaChi: '',
        loiNhan: ''
    },
    orderCode: ''
}

const orderReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_TYPE:
            state.order.loaiPizza = action.payload
            break

        case UPDATE_SIZE:
            const sizeInfo = {
                kichCo: action.payload
            }
            switch (action.payload) {
                case 'S':
                    sizeInfo.duongKinh = '20'
                    sizeInfo.suon = '2'
                    sizeInfo.salad = '200'
                    sizeInfo.soLuongNuoc = '2'
                    sizeInfo.thanhTien = '150000'
                    break
                case 'M':
                    sizeInfo.duongKinh = '25'
                    sizeInfo.suon = '4'
                    sizeInfo.salad = '300'
                    sizeInfo.soLuongNuoc = '3'
                    sizeInfo.thanhTien = '200000'
                    break
                case 'L':
                    sizeInfo.duongKinh = '30'
                    sizeInfo.suon = '8'
                    sizeInfo.salad = '500'
                    sizeInfo.soLuongNuoc = '4'
                    sizeInfo.thanhTien = '250000'
                    break
                default:
                    break
            }
            state.order = {
                ...state.order,
                ...sizeInfo
            }
            break

        case UPDATE_ORDER:
            state.order = {
                ...state.order,
                ...action.payload
            }
            break

        case UPDATE_DRINK:
            state.order.idLoaiNuocUong = action.payload
            break

        case UPDATE_ORDER_CODE:
            state.orderCode = action.payload
            break

        default:
            break
    }

    return { ...state }
}

export default orderReducer
