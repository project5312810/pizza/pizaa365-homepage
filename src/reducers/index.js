
import { combineReducers } from "redux"

import orderReducer from "./order.reducers"

const rootReducer = combineReducers({
    orderReducer
})

export default rootReducer
