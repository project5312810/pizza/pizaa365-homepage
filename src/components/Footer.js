import { Button, Grid, Typography } from "@mui/material"
import { FaArrowUp, FaFacebookSquare, FaInstagramSquare, FaLinkedin, FaPinterestSquare, FaSnapchatSquare, FaTwitterSquare } from 'react-icons/fa'

function Footer() {
    const handleGoToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        })
    }

    return (
        <Grid textAlign='center' py={5} sx={{ backgroundColor: '#ed6c02' }}>
            <Typography variant="h3" my={2}>Footer</Typography>
            <Button
                variant="contained"
                sx={{ 
                    backgroundColor: '#343a40', 
                    textTransform: 'capitalize'
                }}
                onClick={handleGoToTop}
            >
                <FaArrowUp />&ensp;
                To the top
            </Button>
            <Grid my={2}>
                <FaFacebookSquare />
                <FaInstagramSquare />
                <FaSnapchatSquare />
                <FaPinterestSquare />
                <FaTwitterSquare />
                <FaLinkedin />

            </Grid>
            <Typography variant="h6">Powered by DEVCAMP</Typography>
        </Grid>
    )
}

export default Footer