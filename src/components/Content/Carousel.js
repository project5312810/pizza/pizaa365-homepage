import { Swiper, SwiperSlide } from "swiper/react"
import { Navigation, Pagination } from "swiper"
import { Box, styled, Typography } from "@mui/material"

import "swiper/css"
import "swiper/css/navigation"
import "swiper/css/pagination"

function Carousel() {
    var slideImages = [
        {
            name: 'slide1',
            url: 'http://localhost:8000/assets/img/1.jpg',
        },
        {
            name: 'slide2',
            url: 'http://localhost:8000/assets/img/2.jpg',
        },
        {
            name: 'slide3',
            url: 'http://localhost:8000/assets/img/3.jpg',
        },
        {
            name: 'slide4',
            url: 'http://localhost:8000/assets/img/4.jpg',
        }
    ]

    const StyledTypography = styled(Typography)(({ theme }) => ({
        color: theme.palette.warning.main
    }))

    return (
        <div>
            <Box py={5}>
                <StyledTypography variant='h1' fontSize='40px' fontWeight={500}>
                    Pizza 365
                </StyledTypography>
                <Typography fontStyle='italic' className='text-orange'>Truly italian !</Typography>
            </Box>
            <Swiper
                navigation={true}
                pagination={true}
                speed={800}
                loop={true}
                modules={[Navigation, Pagination]}
            >
                {
                    slideImages.map(image => (
                        <SwiperSlide key={image.name}>
                            <img src={image.url} alt={image.name} width='100%' />
                        </SwiperSlide>
                    ))
                }
            </Swiper>
        </div>
    )
}

export default Carousel;