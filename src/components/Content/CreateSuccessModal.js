import { RiCloseLine } from 'react-icons/ri'
import { Grid, Modal, Paper, TextField, Typography } from "@mui/material"
import { useSelector } from 'react-redux'

function CreateSuccessModal({ open, handleClose }) {
    const { orderCode } = useSelector(reduxData => reduxData.orderReducer)

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 600,
        maxWidth: '90%',
        backgroundColor: 'white',
        padding: '20px',
    }

    return (
        <Modal
            open={open}
            onClose={handleClose}
        >
            <Paper elevation={0} style={style}>
                <Grid display='flex' alignItems='center' mb={5}>
                    <Typography variant="h5" className='text-orange' flex={1}>Cảm ơn bạn đã đặt hàng tại Pizza 365. Mã đơn hàng của bạn là:</Typography>
                    <RiCloseLine onClick={handleClose} cursor='pointer' />
                </Grid>
                <Grid>
                    <TextField
                        disabled
                        value={orderCode}
                        label='Mã đơn hàng'
                        variant="outlined"
                        fullWidth
                    />
                </Grid>
            </Paper>
        </Modal>
    )
}

export default CreateSuccessModal