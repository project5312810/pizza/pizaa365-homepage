import { Container, Grid, Typography } from "@mui/material"
import SectionHeading from "./SectionHeading"

function Info() {
    return (
        <Grid py={5}>
            <Container>
                <SectionHeading title='Tại sao lại Pizza 365' />
                <Grid container mt={4}>
                    <Grid item md={3} sm={12} p={4} sx={{ backgroundColor: 'lightgoldenrodyellow', border: '1px solid orange' }}>
                        <Typography variant='h3' pb={2} fontSize='1.75rem' fontWeight={500}>Đa dạng</Typography>
                        <Typography>
                            Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.
                        </Typography>
                    </Grid>
                    <Grid item md={3} sm={12} p={4} sx={{ backgroundColor: 'yellow', border: '1px solid orange' }}>
                        <Typography variant='h3' pb={2} fontSize='1.75rem' fontWeight={500}>Chất lượng</Typography>
                        <Typography>
                            Nguyên liệu sạch 100% rõ nguồn gốc, quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.
                        </Typography>
                    </Grid>
                    <Grid item md={3} sm={12} p={4} sx={{ backgroundColor: 'lightsalmon', border: '1px solid orange' }}>
                        <Typography variant='h3' pb={2} fontSize='1.75rem' fontWeight={500}>Hương vị</Typography>
                        <Typography>
                            Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trải nghiệm từ Pizza 365.
                        </Typography>
                    </Grid>
                    <Grid item md={3} sm={12} p={4} sx={{ backgroundColor: 'orange', border: '1px solid orange' }}>
                        <Typography variant='h3' pb={2} fontSize='1.75rem' fontWeight={500}>Dịch vụ</Typography>
                        <Typography>
                            Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến.
                        </Typography>
                    </Grid>
                </Grid>
            </Container>
        </Grid>
    )
}

export default Info;