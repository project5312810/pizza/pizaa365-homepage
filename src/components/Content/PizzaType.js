import { Button, Card, CardActions, CardContent, CardMedia, Container, Grid, Typography } from "@mui/material"
import SectionHeading from "./SectionHeading"

import { useDispatch, useSelector } from "react-redux"
import { updateType } from "../../actions/order.actions"
import images from "../../assets/images"

function PizzaType() {
    const dispatch = useDispatch()
    const { order } = useSelector(reduxData => reduxData.orderReducer)

    const handleChooseType = type => {
        dispatch(updateType(type))
        console.log(type)
    }

    return (
        <Grid py={5}>
            <Container>
                <SectionHeading title='Chọn loại pizza' />
                <Grid container mt={4} spacing={3}>
                    <Grid item xs={12} sm={6} md={4}>
                        <Card>
                            <CardMedia
                                component="img"
                                image={images.seafood}
                                alt="hai san"
                            />
                            <CardContent>
                                <Typography variant="h5" textTransform='uppercase'>Ocean mania</Typography>
                                <Typography textTransform='uppercase' py={2}>Pizza hải sản xốt mayonnaise</Typography>
                                <Typography>Xốt Cà Chua, Phô Mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây</Typography>
                            </CardContent>
                            <CardActions>
                                <Button
                                    variant="contained"
                                    color={order.loaiPizza === 'SEAFOOD' ? 'success' : 'warning'}
                                    fullWidth
                                    sx={{ color: 'rgba(0, 0, 0, 0.87)', fontWeight: '700', textTransform: 'capitalize' }}
                                    onClick={() => handleChooseType('SEAFOOD')}
                                >
                                    Chọn
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                        <Card>
                            <CardMedia
                                component="img"
                                image={images.hawaiian}
                                alt="hai san"
                            />
                            <CardContent>
                                <Typography variant="h5" textTransform='uppercase'>Hawaiian</Typography>
                                <Typography textTransform='uppercase' py={2}>Pizza dăm bông dứa kiểu hawaii</Typography>
                                <Typography>Xốt cà chua, phô mai mozzarella, thịt dăm bông, thơm</Typography>
                            </CardContent>
                            <CardActions>
                                <Button
                                    variant="contained"
                                    color={order.loaiPizza === 'HAWAII' ? 'success' : 'warning'}
                                    fullWidth
                                    sx={{ color: 'rgba(0, 0, 0, 0.87)', fontWeight: '700', textTransform: 'capitalize' }}
                                    onClick={() => handleChooseType('HAWAII')}
                                >
                                    Chọn
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                        <Card>
                            <CardMedia
                                component="img"
                                image={images.bacon}
                                alt="hai san"
                            />
                            <CardContent>
                                <Typography variant="h5" textTransform='uppercase'>Cheesy chicken bacon</Typography>
                                <Typography textTransform='uppercase' py={2}>Pizza gà phô mai thị heo xông khói</Typography>
                                <Typography>Xốt phô mai, thịt gà, thịt heo muối, phô mai mozzarella, cà chua</Typography>
                            </CardContent>
                            <CardActions>
                                <Button
                                    variant="contained"
                                    color={order.loaiPizza === 'BACON' ? 'success' : 'warning'}
                                    fullWidth
                                    sx={{ color: 'rgba(0, 0, 0, 0.87)', fontWeight: '700', textTransform: 'capitalize' }}
                                    onClick={() => handleChooseType('BACON')}
                                >
                                    Chọn
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </Grid>
    )
}

export default PizzaType