import PropTypes from 'prop-types'
import { useDispatch, useSelector } from "react-redux"
import { useState } from "react"
import { Button, Container, Grid, Typography, OutlinedInput } from "@mui/material"

import { updateOrder } from "../../actions/order.actions"
import SectionHeading from "./SectionHeading"

function FormSubmit({ submit }) {
    const dispatch = useDispatch()
    const { order } = useSelector(reduxData => reduxData.orderReducer)
    const [nameValue, setNameValue] = useState(order.hoTen)
    const [emailValue, setEmailValue] = useState(order.email)
    const [phoneValue, setPhoneValue] = useState(order.soDienThoai)
    const [addressValue, setAddressValue] = useState(order.diaChi)
    const [voucherValue, setVoucherValue] = useState(order.idVoucher)
    const [messageValue, setMessageValue] = useState(order.loiNhan)

    const handleSendData = e => {
        e.preventDefault()

        const formData = {
            hoTen: nameValue,
            email: emailValue,
            soDienThoai: phoneValue,
            diaChi: addressValue,
            idVoucher: voucherValue,
            loiNhan: messageValue
        }
        console.log(formData)

        dispatch(updateOrder(formData))

        if (!order.kichCo) {
            alert('Bạn chưa chọn kích cỡ!')
        } else if (!order.loaiPizza) {
            alert('Bạn chưa chọn loại pizza!')
        } else if (!order.idLoaiNuocUong) {
            alert('Bạn chưa chọn loại nước uống!')
        } else {
            submit()
        }
    }

    return (
        <Grid py={5}>
            <Container>
                <SectionHeading title='Gửi đơn hàng' />
                <Grid mt={4}>
                    <form onSubmit={handleSendData}>
                        <Typography py={1}>Tên</Typography>
                        <OutlinedInput
                            value={nameValue}
                            onChange={e => setNameValue(e.target.value)}
                            placeholder="Nhập tên của bạn"
                            variant="outlined"
                            fullWidth
                            required
                        />
                        <Typography py={1}>Email</Typography>
                        <OutlinedInput
                            value={emailValue}
                            onChange={e => setEmailValue(e.target.value)}
                            placeholder="Nhập email"
                            type='email'
                            variant="outlined"
                            fullWidth
                            required
                        />
                        <Typography py={1}>Số điện thoại</Typography>
                        <OutlinedInput
                            value={phoneValue}
                            onChange={e => setPhoneValue(e.target.value)}
                            placeholder="Nhập số điện thoại"
                            variant="outlined"
                            fullWidth
                            required
                        />
                        <Typography py={1}>Địa chỉ</Typography>
                        <OutlinedInput
                            value={addressValue}
                            onChange={e => setAddressValue(e.target.value)}
                            placeholder="Nhập địa chỉ"
                            variant="outlined"
                            fullWidth
                            required
                        />
                        <Typography py={1}>Mã giảm giá</Typography>
                        <OutlinedInput
                            value={voucherValue}
                            onChange={e => setVoucherValue(e.target.value)}
                            placeholder="Nhập mã giảm giá"
                            variant="outlined"
                            fullWidth
                        />
                        <Typography py={1}>Lời nhắn</Typography>
                        <OutlinedInput
                            value={messageValue}
                            onChange={e => setMessageValue(e.target.value)}
                            placeholder="Nhập lời nhắn"
                            variant="outlined"
                            fullWidth
                        />
                        <Button variant="contained" type='submit' color="warning" sx={{ marginBlock: '1rem' }} fullWidth>Gửi</Button>
                    </form>
                </Grid>
            </Container>
        </Grid>
    )
}

FormSubmit.propTypes = {
    submit: PropTypes.func
}

export default FormSubmit;