import axios from "axios"
import { useDispatch, useSelector } from "react-redux"
import { useEffect, useState } from "react"
import { Container, FormControl, Grid, InputLabel, MenuItem, Select } from "@mui/material"

import { updateDrink } from "../../actions/order.actions"
import SectionHeading from "./SectionHeading"

function DrinkSelect() {
    const dispatch = useDispatch()
    const { order } = useSelector(reduxData => reduxData.orderReducer)
    const [drinkList, setDrinkList] = useState([])

    const handleChange = e => {
        dispatch(updateDrink(e.target.value))
        console.log(e.target.value)
    }

    useEffect(() => {
        const getDrinkList = async () => {
            const result = await axios('http://localhost:8000/api/drinks')
            setDrinkList(result.data)
        }
        getDrinkList()
    }, [])

    return (
        <Grid py={5}>
            <Container>
                <SectionHeading title='Chọn đồ uống' />
                <Grid mt={4}>
                    <FormControl fullWidth>
                        <InputLabel id="demo-simple-select-label">Chọn đồ uống</InputLabel>
                        <Select
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={order.idLoaiNuocUong}
                            label="Chọn đồ uống"
                            onChange={handleChange}
                        >
                            {drinkList.map(drink => (
                                <MenuItem key={drink._id} value={drink.maNuocUong}>{drink.tenNuocUong}</MenuItem>
                            ))}
                        </Select>
                    </FormControl>
                </Grid>
            </Container>
        </Grid>
    )
}

export default DrinkSelect