import { Grid, Typography } from '@mui/material'
import PropTypes from 'prop-types'

function SectionHeading({ title, subtitle }) {
    return (
        <Grid className='text-orange' textAlign='center'>
            <Typography
                variant='h2'
                fontSize='2rem'
                fontWeight={500}
                borderBottom='1px solid #ed6c02'
                display='inline-block'
                py={1}
            >
                {title}
            </Typography>
            {subtitle &&
                <>
                    <Typography py={2}>{subtitle}</Typography>
                </>
            }
        </Grid>
    )
}

SectionHeading.propTypes = {
    title: PropTypes.string.isRequired,
    subtitle: PropTypes.string
}

export default SectionHeading;