import PropTypes from 'prop-types'
import axios from "axios"
import { useEffect, useState } from "react"
import { RiCloseLine } from 'react-icons/ri'
import { Button, Grid, Modal, Paper, TextField, Typography } from "@mui/material"
import { useDispatch, useSelector } from "react-redux"
import { updateOrder, updateOrderCode } from "../../actions/order.actions"

function PreviewOrderModal({ open, handleClose, handleCreateSuccess }) {
    const dispatch = useDispatch()
    const [detail, setDetail] = useState('')
    const [discount, setDiscount] = useState(0)
    const { order } = useSelector(reduxData => reduxData.orderReducer)

    useEffect(() => {
        if (order.idVoucher) {
            const checkVoucher = async () => {
                const result = await axios('http://localhost:8000/api/vouchers/' + order.idVoucher)
                if (result.data) {
                    setDiscount(result.data.data.phanTramGiamGia)
                } else {
                    dispatch(updateOrder({ idVoucher: '' }))
                }
                setDetail(
                    `Xác nhận: ${order.hoTen}, ${order.soDienThoai}, ${order.diaChi}
Menu: ${order.kichCo}, sườn ${order.suon}, nước ${order.soLuongNuoc}, salad ${order.salad}g
Loại pizza: ${order.loaiPizza}, giá: ${order.thanhTien}${order.idVoucher ? ', mã giảm giá: ' + order.idVoucher : ''}
Phải thanh toán: ${order.thanhTien - order.thanhTien * discount / 100} vnd${discount ? ' (giảm giá ' + discount + '%)' : ''}`
                )
            }
            checkVoucher()
        }
    }, [order, discount, dispatch])


    const handleCreateOrder = () => {
        const createOrder = async () => {
            const result = await axios.post('http://localhost:8000/api/orders', order)
            dispatch(updateOrderCode(result.data.orderCode))
        }
        createOrder()
        handleClose()
        handleCreateSuccess()
    }

    const style = {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        width: 600,
        maxWidth: '90%',
        backgroundColor: 'white',
        padding: '20px',
    }

    return (
        <Modal
            open={open}
            onClose={handleClose}
        >
            <Paper elevation={0} style={style}>
                <Grid display='flex' alignItems='center'>
                    <Typography variant="h4" flex={1}>Thông tin đơn hàng</Typography>
                    <RiCloseLine onClick={handleClose} cursor='pointer' />
                </Grid>
                <Grid>
                    <Typography py={1}>Tên</Typography>
                    <TextField
                        disabled
                        value={order.hoTen}
                        variant="outlined"
                        fullWidth

                    />
                    <Typography py={1}>Email</Typography>
                    <TextField
                        disabled
                        value={order.email}
                        variant="outlined"
                        fullWidth
                    />
                    <Typography py={1}>Số điện thoại</Typography>
                    <TextField
                        disabled
                        value={order.soDienThoai}
                        variant="outlined"
                        fullWidth
                    />
                    <Typography py={1}>Địa chỉ</Typography>
                    <TextField
                        disabled
                        value={order.diaChi}
                        variant="outlined"
                        fullWidth
                    />
                    <Typography py={1}>Mã giảm giá</Typography>
                    <TextField
                        disabled
                        value={order.idVoucher}
                        variant="outlined"
                        fullWidth
                    />
                    <Typography py={1}>Lời nhắn</Typography>
                    <TextField
                        disabled
                        value={order.loiNhan}
                        variant="outlined"
                        fullWidth
                    />
                    <Typography py={1}>Thông tin chi tiết</Typography>
                    <TextField
                        disabled
                        multiline
                        value={detail}
                        variant="outlined"
                        fullWidth
                    />
                    <Button variant="contained" color="warning" sx={{ marginBlock: '1rem' }} fullWidth onClick={handleCreateOrder}>Gửi</Button>
                </Grid>
            </Paper>
        </Modal>
    )
}

PreviewOrderModal.propTypes = {
    open: PropTypes.bool.isRequired,
    handleClose: PropTypes.func.isRequired,
    handleCreateSuccess: PropTypes.func
}

export default PreviewOrderModal;