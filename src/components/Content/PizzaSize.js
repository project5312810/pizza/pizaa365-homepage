import { Button, Card, CardActions, CardContent, CardHeader, Container, Divider, Grid, Typography } from "@mui/material"
import SectionHeading from "./SectionHeading"

import { useDispatch, useSelector } from "react-redux"
import { updateSize } from "../../actions/order.actions"
function PizzaSize() {
    const dispatch = useDispatch()
    const { order } = useSelector(reduxData => reduxData.orderReducer)

    const handleChooseSize = size => {
        dispatch(updateSize(size))
        console.log(size)
    }


    return (
        <Grid py={5}>
            <Container>
                <SectionHeading title='Chọn size pizza' subtitle='Chọn combo pizza phù hợp với nhu cầu của bạn.' />
                <Grid container mt={4} spacing={3}>
                    <Grid item xs={12} sm={6} md={4}>
                        <Card sx={{ textAlign: 'center' }}>
                            <CardHeader
                                title={
                                    <Typography variant="h5" fontWeight={500}>{'S (small)'}</Typography>
                                }
                                sx={{ backgroundColor: '#ed6c02' }}
                            />
                            <CardContent>
                                <Typography py={2}>Đường kính: <b>20cm</b></Typography>
                                <Divider />
                                <Typography py={2}>Sườn nướng: <b>2</b></Typography>
                                <Divider />
                                <Typography py={2}>Salad: <b>200g</b></Typography>
                                <Divider />
                                <Typography py={2}>Nước ngọt: <b>2</b></Typography>
                                <Divider />
                                <Typography variant="h3" fontWeight={500} py={1}>150.000</Typography>
                                <Typography>VND</Typography>

                            </CardContent>
                            <CardActions>
                                <Button
                                    variant="contained"
                                    color={order.kichCo === 'S' ? 'success' : 'warning'}
                                    fullWidth
                                    sx={{ color: 'rgba(0, 0, 0, 0.87)', fontWeight: '700', textTransform: 'capitalize' }}
                                    onClick={() => handleChooseSize('S')}
                                >
                                    Chọn
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                        <Card sx={{ textAlign: 'center' }}>
                            <CardHeader
                                title={
                                    <Typography variant="h5" fontWeight={500}>{'S (small)'}</Typography>
                                }
                                sx={{ backgroundColor: '#ed6c02' }}
                            />
                            <CardContent>
                                <Typography py={2}>Đường kính: <b>25cm</b></Typography>
                                <Divider />
                                <Typography py={2}>Sườn nướng: <b>4</b></Typography>
                                <Divider />
                                <Typography py={2}>Salad: <b>300g</b></Typography>
                                <Divider />
                                <Typography py={2}>Nước ngọt: <b>3</b></Typography>
                                <Divider />
                                <Typography variant="h3" fontWeight={500} py={1}>200.000</Typography>
                                <Typography>VND</Typography>

                            </CardContent>
                            <CardActions>
                                <Button
                                    variant="contained"
                                    color={order.kichCo === 'M' ? 'success' : 'warning'}
                                    fullWidth
                                    sx={{ color: 'rgba(0, 0, 0, 0.87)', fontWeight: '700', textTransform: 'capitalize' }}
                                    onClick={() => handleChooseSize('M')}
                                >
                                    Chọn
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                    <Grid item xs={12} sm={6} md={4}>
                        <Card sx={{ textAlign: 'center' }}>
                            <CardHeader
                                title={
                                    <Typography variant="h5" fontWeight={500}>{'S (small)'}</Typography>
                                }
                                sx={{ backgroundColor: '#ed6c02' }}
                            />
                            <CardContent>
                                <Typography py={2}>Đường kính: <b>30cm</b></Typography>
                                <Divider />
                                <Typography py={2}>Sườn nướng: <b>8</b></Typography>
                                <Divider />
                                <Typography py={2}>Salad: <b>500g</b></Typography>
                                <Divider />
                                <Typography py={2}>Nước ngọt: <b>4</b></Typography>
                                <Divider />
                                <Typography variant="h3" fontWeight={500} py={1}>250.000</Typography>
                                <Typography>VND</Typography>

                            </CardContent>
                            <CardActions>
                                <Button
                                    variant="contained"
                                    color={order.kichCo === 'L' ? 'success' : 'warning'}
                                    fullWidth
                                    sx={{ color: 'rgba(0, 0, 0, 0.87)', fontWeight: '700', textTransform: 'capitalize' }}
                                    onClick={() => handleChooseSize('L')}
                                >
                                    Chọn
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                </Grid>
            </Container>
        </Grid>
    )
}

export default PizzaSize;