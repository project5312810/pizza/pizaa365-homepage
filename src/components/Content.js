import { useState } from 'react'
import Carousel from "./Content/Carousel";
import Info from "./Content/Info";
import PizzaSize from "./Content/PizzaSize";
import { styled } from '@mui/material/styles'
import { Container } from "@mui/material"
import PizzaType from "./Content/PizzaType";
import DrinkSelect from "./Content/DrinkSelect";
import FormSubmit from "./Content/FormSubmit";
import PreviewOrderModal from './Content/PreviewOrderModal'
import CreateSuccessModal from './Content/CreateSuccessModal'
function Content() {
    const [isOpenPreviewModal, setIsOpenPreviewModal] = useState(false)
    const [isOpenSuccessModal, setIsOpenSuccessModal] = useState(false)

    const StyledContainer = styled(Container)({
        marginTop: '68.5px'
    })

    const handleSubmit = () => {
        setIsOpenPreviewModal(true)
    }
    return (
        <StyledContainer>
            <Carousel />
            <Info />
            <PizzaSize />
            <PizzaType />
            <DrinkSelect />
            <FormSubmit submit={handleSubmit} />
            <PreviewOrderModal open={isOpenPreviewModal} handleClose={() => setIsOpenPreviewModal(false)} handleCreateSuccess={() => setIsOpenSuccessModal(true)} />
            <CreateSuccessModal open={isOpenSuccessModal} handleClose={() => setIsOpenSuccessModal(false)} />

        </StyledContainer >
    )
}
export default Content;